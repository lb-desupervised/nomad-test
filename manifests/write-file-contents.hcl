# An example batch job which echoes the contents of a file.

job "write-file" {
  datacenters = ["dc1"]
  type = "batch"

  group "writer" {
    count = 1

    ephemeral_disk {
      size = 120
    }

    task "write-to-file" {
      driver = "docker"

      config {
        image = "alpine:latest"
        args = ["sh", "-c", "echo 'Hello from a job!' > /mnt/myvolume/myfile"]
      }

      volume_mount {
        volume      = "myvolume"
        destination = "/mnt/myvolume"
      }

      resources {
        cpu    = 500 # 500 MHz
        memory = 256 # 256MB
      }
    }

    volume "myvolume" {
      type = "host"
      read_only = false
      source = "myvolume"
    }
  }
}
