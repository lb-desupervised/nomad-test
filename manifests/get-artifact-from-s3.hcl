# An example batch job which echoes the contents of a file.

job "echo-file2" {
  datacenters = ["dc1"]
  type = "batch"

  group "echo" {
    count = 1

    restart {
      attempts = 2
      interval = "30m"
      delay = "15s"
      mode = "fail"
    }

    ephemeral_disk {
      size = 120
    }

    task "echo" {
      driver = "docker"

      config {
        image = "alpine:latest"
        args = ["sh", "-c", "cat /mnt/myvolume/myfile"]
      }

      volume_mount {
        volume      = "myvolume"
        destination = "/mnt/myvolume"
      }

      resources {
        cpu    = 500 # 500 MHz
        memory = 256 # 256MB
      }
    }

    volume "myvolume" {
      type = "host"
      read_only = true
      source = "myvolume"
    }
  }
}
