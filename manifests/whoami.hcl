# Run a whoami server which returns information about the incoming request.

job "whoami-app" {
  datacenters = ["dc1"]

  group "whoami" {
    count = 1

    network {
      port  "http" {
        to = -1
      }
    }

    service {
      name = "whoami"
      port = "http"

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.whoami.rule=Path(`/whoami`)",
        "traefik.http.routers.whoami.middlewares=add-user-id-header@consulcatalog",
        "traefik.http.middlewares.add-user-id-header.headers.customrequestheaders.Alvis-User-Id=abcdef",
      ]

      check {
        type     = "http"
        path     = "/"
        interval = "2s"
        timeout  = "2s"
      }
    }

    task "server" {
      driver = "docker"

      config {
        image = "containous/whoami:latest"
        ports = ["http"]
      }
    }
  }
}
