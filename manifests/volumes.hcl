# Use a local path as a volume in Nomad.

client {
  host_volume "myvolume" {
    path      = "/vagrant/myvolume"
    read_only = false
  }
}

