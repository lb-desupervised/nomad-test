# An example batch job which just echoes some stuff.

job "echo" {
  datacenters = ["dc1"]
  type = "batch"

  group "echo" {
    count = 1

    restart {
      attempts = 2
      interval = "30m"
      delay = "15s"
      mode = "fail"
    }

    ephemeral_disk {
      size = 120
    }

    task "echo" {
      driver = "docker"

      config {
        image = "alpine:latest"
        args = ["sh", "-c", "echo hello ajdshfkjakdjasdh"]
      }

      resources {
        cpu    = 500 # 500 MHz
        memory = 256 # 256MB
      }
    }
  }
}
