"""
Submit a job which just uses an alpine image and echo some text.

Requirements:
    - `python-nomad`
    - Running nomad agent at `0.0.0.0`
"""

import uuid

import nomad

# TODO: Look into ParentID and Payload

JOB_ID = str(uuid.uuid4())
JOB_CONFIG = {
    "Job": {
        "Datacenters": ["dc1"],
        "ID": JOB_ID,
        "Name": JOB_ID,
        "TaskGroups": [
            {
                "Count": 1,
                "EphemeralDisk": {"SizeMB": 120},
                "Name": "group-whatever",
                "Tasks": [
                    {
                        "Config": {
                            "image": "alpine:latest",
                            "args": ["sh", "-c", "echo Hello from python"],
                        },
                        "Driver": "docker",
                        "Name": "echo",
                        "Resources": {
                            "CPU": 500,
                            "MemoryMB": 120,
                        },
                    }
                ],
            }
        ],
        "Type": "batch",
    }
}


def main():
    my_nomad = nomad.Nomad(host="0.0.0.0")
    response = my_nomad.job.register_job(JOB_ID, JOB_CONFIG)
    print(response)


if __name__ == "__main__":
    main()
