## Basic Usage

For a real introduction, see the Hashicorp docs:
https://learn.hashicorp.com/nomad

Start a dev agent:

```
sudo nomad agent -dev -bind 0.0.0.0 -log-level INFO
```

Create an example job (creates `example.hcl`):

```
nomad job init
```

Run a job:

```
nomad job run <FILE>
```

UI is available at `localhost:4646`.

## Copy files from VM

Install `scp` plugin:

```
vagrant plugin install vagrant-scp
```

Copy a file:

```
vagrant scp <VM ID (use vagrant global-status)>:<VM path> <Local path>
```

*NOTE THAT THE LOCAL DIRECTORY THAT VAGRANT IS LAUNCHED FROM IS BY DEFAULT
SHARED TO THE LOCATION `/vagrant` IN THE VM.*

## Connect from localhost

In order to connect to the services running on the Vagrant machine, we need to
expose any expected ports (NB this is a consequence of using nomad in a VM, not
something that we'll have to deal with on real deployments). To do this, the
vagrant machine can be configured with extra lines for each port:

```
config.vm.network "forwarded_port", guest: <GUEST PORT>, host: <HOST PORT>, auto_correct: true, host_ip: "127.0.0.1"
```

## Mount Volume in a Job

The volume needs to be registered with `nomad` beforehand (this might not always
be the case). This can be done by specifying a `-config` option when starting
the server. For example:

```
sudo nomad agent -dev -bind 0.0.0.0 -log-level INFO -config manifests/volumes.hcl
```

(See `manifests/volumes.hcl`)

See `manifests/echo-file-contents.hcl` as an example batch job that uses this
volume.

## Traefik

See `manifests/traefik.hcl` for an example traefik setup.

Rules are added to services using the `job.group.service.tags` array. For
example:

```
job "whoami-app" {
  ...
  group "whoami" {
    ...
    service {
      ...
      tags = [
        "traefik.enable=true",
        "traefik.http.routers.whoami.rule=Path(`/whoami`)",
        "traefik.http.routers.whoami.middlewares=add-user-id-header@consulcatalog",
        "traefik.http.middlewares.add-user-id-header.headers.customrequestheaders.Alvis-User-Id=abcdef",
      ]
    }
  }
}
```

## Traefik and other monitoring tools

https://atodorov.me/2021/03/27/using-traefik-on-nomad/

## Troubleshooting

Sometimes the virtualbox has internet issues and can't pull any docker images,
meaning that all jobs using docker fail. This can be fixed by updating
`nameserver` to `8.8.8.8`:

```
sudo vim /etc/netplan/01-netcfg.yaml
```

Make sure ethernets has a nameserver pointing to 8.8.8.8, for example:

```
network:
  version: 2
  ethernets:
    eth0:
      dhcp4: true
      nameservers:
        addresses: [8.8.8.8]
```

Apply updates:

```
sudo netplan apply
```
